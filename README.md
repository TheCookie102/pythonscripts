Learning Python
===============

## About

This is just a little Git repo for me to commit the various scripts I create while learning Python

## Licensing

Unless said otherwise, my code is generally licensed under the MIT License. See LICENSE for more information
