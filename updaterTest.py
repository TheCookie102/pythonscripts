#-------------------------------------------------------------------------------
# Name:        Update checker test 
# Language:    Python
# Version:     DEVTEST-0
# Purpose:     Test framework to work on updater system for future scripts
#
# Author:      Xerxes
#
# Created:     27/03/2013
# Copyright:   N/A
# Licence:     MIT Licence (see bottom of file)
#-------------------------------------------------------------------------------

import sys
import time
import urllib2

def checkUpdates():
	print('Checking for updates...')
	print('DEBUG: Checking for updates...')
	try:
		fh = urllib2.urlopen('http://xerxes.x64.me/updatecheck/updaterTest.txt')
		currVer = fh.read()
	except IOError:
		print('ERROR: Remote file not found! Updater cannot continue!')
		sys.exit(2)
	except ContentTooShortError:
		print('ERROR: Truncated data detected!')
		sys.exit(3)
	except:
		print('An unknown error occured while checking for updates!')
		sys.exit(9)

	# Compare local version strings to remote
	currVer = str(currVer)
	if currVer == version:
		print('Your version (' + str(version) + ') is up to date!')
		sys.exit(0)
		print('DEBUG: == if passed')
	elif currVer != version:
		print('Your version is out of date!')
		print('The latest version is: ' + str(currVer))
		print('Please update from http://xerxes.x64.me/site/scripts/updaterTest')
		sys.exit(0)
	print('DEBUG: != if passed')

version = '0.0.3'
# Version architecture: major.minor.build

print('Update checker test v' + str(version))
print('This script is a developer test, which means it is of no use to the')
print('end user!')
raw_input('Press <enter> to confirm')

checkUpdates()
