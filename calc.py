#-------------------------------------------------------------------------------
# Name:        Python 2.7 Calculator
# Language:    Python
# Version:     v1.4.1
# Purpose:     A calculator made in Python 2.7, utilising more advanced
#              functions of the math module
#
# Author:      Circinus Prime
#
# Created:     02/03/2013
# Copyright:   N/A
# Licence:     MIT Licence (see bottom of file)
#-------------------------------------------------------------------------------

import sys
import math
import time
try:
    import urllib2
    ulib2Found = True
except:
    print('WARNING: URLLib2 failed to load! Updater will not function!')
    print('Recieved exception: ')
    print(sys.exc_type)
    print(sys.exc_value)
    ulib2Found = False

def checkUpdates():
    print('Checking for updates...')
    time.sleep(1)
    if ulib2Found == True:
        try:
            fh = urllib2.urlopen('http://circinusp.uk.to/updatecheck/calc.txt')
            currVer = fh.read()
        except IOError:
            print('UPDATER ERROR: Could not retrieve latest version!')
            print(sys.exc_type)
            print(sys.exc_value)
            time.sleep(2)
        except ContentTooShortError:
            print('UPDATER ERROR: Truncated data detected!')
            print(sys.exc_type)
            print(sys.exc_value)
            time.sleep(2)
        except:
            print('UPDATER ERROR: Unknown error!')
            print('Please give the following information to CircinusPrime: ')
            print(sys.exc_type)
            print(sys.exc_value)
            print(sys.exc_traceback)
            time.sleep(2)
        currVer = str(currVer)
        if currVer == version:
            print('Your version (' + str(version) + ') is up to date!')
            time.sleep(2)
        elif currVer != version:
            print('Your version is outdated!')
            print('A newer version is avaliable: ' + str(currVer))
            print('Please update at: http://circinusp.uk.to/site/scripts/calc')
            time.sleep(2)
            # Note to self: scripts/ is currently a placeholder
    else:
        print('ERROR: URLLib2 not found! Cannot check for updates!')
        time.sleep(1)

def menu(): # Your generic mass print :)
    print('Python Calc v' + str(version))
    print('Main Menu')
    print('Choose what you want to perform: ')
    print('---------------')
    print('1. Addition ')
    print('2. Subtraction')
    print('3. Multiplication')
    print('4. Division')
    print('5. Square root')
    print('6. Sine')
    print('7. Cosine')
    print('8. Tangent')
    print('9. Angle conversion')
    print('10. Exit')
    print('11. Special numbers')
    select() # Each menu calls it's own selection function. These parse user
             # input to find out what menu entry they chose and execute it.
             # See def select() for more info

def angconv():
    print('Angular conversion menu')
    print('---------------')
    print('1. Convert radians to degrees')
    print('2. Convert degrees to radians')
    print('3. Go back to main menu')
    angconvselect()

def select():
    time.sleep(1)
    selection = raw_input('Select an option (1-11): ') # Waits for user to choose
    str(selection) # Converts to string for if-checking
    if selection == '1': # Yay for mass if statements
        add()
    elif selection == '2':
        sub()
    elif selection == '3':
        mult()
    elif selection == '4':
        div()
    elif selection == '5':
        sqrt()
    elif selection == '6':
        sin()
    elif selection == '7':
        cosin()
    elif selection == '8':
        tan()
    elif selection == '9':
        angconv()
    elif selection == '10':
        end()
    elif selection == '11': # Yes, there really is an if for every menu entry :)
        special()
    else: # Triggered if an undefined menu option is selected
        print('Incorrect menu option!')
        time.sleep(2)

def special():
    print('Special numbers menu')
    print('---------------')
    print('1. Pi')
    print('2. E')
    print('3. Go back to main menu')
    specialselect()

def specialselect():
    specialSelection = raw_input('Select an option (1-3): ')
    if specialSelection == '1':
        pi()
    elif specialSelection == '2':
        e()
    elif specialSelection == '3':
        pass

def pi(): # Simple calls to functions in the math module
    try:
        print('Pi is ' + str(math.pi))
        time.sleep(1)
        raw_input('Press <enter> to continue')
    except:
        print('Something broke!')
        print(sys.exc_type)
        print(sys.exc_value)
        print(sys.exc_traceback)
        raw_input('Press <enter> to continue: ')

def e():
    try:
        print('E is ' + str(math.e))
        time.sleep(1)
        print('Press <enter> to continue')
    except:
        print('Something broke!')
        print(sys.exc_type)
        print(sys.exc_value)
        print(sys.exc_traceback)
        raw_input('Press <enter> to continue: ')

def angconvselect():
    angConvSelection = raw_input('Select an option (1-3): ')
    if angConvSelection == '1':
        deg()
    elif angConvSelection == '2':
        rad()
    elif angConvSelection == '3':
        pass

def end():
    print('Thanks for trying my Python calculator!')
    print('Version ' + str(version))
    print('Have an issue? Please report it at: ')
    print('https://bitbucket.org/TheCookie102/pythonscripts/issues')
    time.sleep(1)
    raw_input('Press <enter> to quit')
    sys.exit(0)

def add(): # Converts input to float format for decimal capability
    try:
        print('Note: This calculator works with decimals too!')
        firstNumber = float(raw_input('Enter the first number: '))
        print('Your first number is: ' + str(firstNumber))
        secondNumber = float(raw_input('Enter the second number: '))
        print('Result: ' + str(firstNumber+secondNumber))
        time.sleep(1)
        raw_input('Press <enter> to continue: ')
    except:
        print(sys.exc_type)
        print(sys.exc_value)
        print(sys.exc_traceback)
        raw_input('Press <enter> to continue: ')

def sub(): # These are really similar
    try:
        print('Note: Subtraction sums support decimals!')
        firstNumber = float(raw_input('Enter the first number: '))
        print('Your first number is: ' + str(firstNumber))
        secondNumber = float(raw_input('Enter the second number: '))
        print('Result: ' + str(firstNumber-secondNumber))
        time.sleep(1)
        raw_input('Press <enter> to continue')
    except:
        print('Something broke!')
        print(sys.exc_type)
        print(sys.exc_value)
        print(sys.exc_traceback)
        raw_input('Press <enter> to continue: ')

def div(): # If you can't tell already ;)
    try:
        print('Note: Division sums work with decimals!')
        firstNumber = float(raw_input('Enter the first number: '))
        print('Your first number is: ' + str(firstNumber))
        secondNumber = float(raw_input('Enter the second number: '))
        print('Result: ' + str(firstNumber/secondNumber))
        time.sleep(1)
        raw_input('Press <enter> to continue')
    except:
        print('Something broke!')
        print(sys.exc_type)
        print(sys.exc_value)
        print(sys.exc_traceback)
        raw_input('Press <enter> to continue: ')

def mult(): # Why are you still reading this?
    try:
        print('Note: Multiplication sums support decimals!')
        firstNumber = float(raw_input('Enter the first number: '))
        print('Your first number is: ' + str(firstNumber))
        secondNumber = float(raw_input('Enter the second number: '))
        print('Result: ' + str(firstNumber*secondNumber))
        time.sleep(1)
        raw_input('Press <enter> to continue')
    except:
        print('Something broke! Onoes')
        print(sys.exc_type)
        print(sys.exc_value)
        print(sys.exc_traceback)
        raw_input('Press <enter> to continue: ')

def sqrt():
    try:
        number = float(raw_input('Enter the number you wish to square root: '))
        print('Result: ' + str(math.sqrt(number)))
        time.sleep(1)
        raw_input('Press <enter> to continue')
    except:
        print('Something broke!')
        print(sys.exc_type)
        print(sys.exc_value)
        print(sys.exc_traceback)
        raw_input('Press <enter> to continue: ')

def sin(): # Simple math functions, makes my life a lot easier
    try:
        number = float(raw_input('Enter the number you wish to find the sine of: '))
        print('Result: ' + str(math.sin(number)))
        time.sleep(1)
        raw_input('Press <enter> to continue')
    except:
        print('Something broke!')
        print(sys.exc_type)
        print(sys.exc_value)
        print(sys.exc_traceback)
        raw_input('Press <enter> to continue: ')

def cosin():
    try:
        number = float(raw_input('Enter the number you wish to find the cosine of: '))
        print('Result: ' + str(math.cos(number)))
        time.sleep(1)
        raw_input('Press <enter> to continue')
    except:
        print('Something broke!')
        print(sys.exc_type)
        print(sys.exc_value)
        print(sys.exc_traceback)
        raw_input('Press <enter> to continue: ')

def tan():
    try:
        number = float(raw_input('Enter the number you wish to find the tangent of: '))
        print('Result: ' + str(math.tan(number)))
        time.sleep(1)
        raw_input('Press <enter> to continue')
    except:
        print('Something broke!')
        print(sys.exc_type)
        print(sys.exc_value)
        print(sys.exc_traceback)
        raw_input('Press <enter> to continue: ')

def deg():
    try:
        print('Converting radians to degrees (angular conversion)')
        number = float(raw_input('Enter the number of radians to convert: '))
        print('Result: ' + str(math.degrees(number)))
        time.sleep(1)
        raw_input('Press <enter> to continue')
    except:
        print('Something broke!')
        print(sys.exc_type)
        print(sys.exc_value)
        print(sys.exc_traceback)
        raw_input('Press <enter> to continue: ')

def rad():
    try:
        print('Converting degrees to radians (angular conversion)')
        number = float(raw_input('Enter number of degrees to convert: '))
        print('Result: ' + str(math.radians(number)))
        time.sleep(1)
        raw_input('Press <enter> to continue')
    except:
        print('Something broke! Error info below: ')
        print(sys.exc_type)
        print(sys.exc_value)
        print(sys.exc_traceback)
        raw_input('Press <enter> to continue: ')


print('Licenced under the MIT Licence; see Git or source code of this script for details')
time.sleep(1) # Gotta have licence awareness

version = '1.4.1'
# Version algorithm: major.minor.build

checkUpdates()

while True: # Anti memory leak loop; whenever a def finishes, it returns to
    menu()  # this loop, which re-prints the menu

# Legal stuff: The MIT Licence
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
#IN THE SOFTWARE.
